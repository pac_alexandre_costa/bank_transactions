var assert = require('assert');
var mongoose = require('mongoose');

var environment = require('../utils/commons').config();
var Person = require('../models/person');
var PersonController = require('../controllers/person');

before(function(done){
  mongoose.connect(environment.urlDB, function(err){
    if(err){
      done(err);
      return;
    }
    // let's ensure the test database is empty
    Person.remove({}, done);
  });
});

after(function(){
  // disconnect to avoid hanged server
  mongoose.disconnect();
});

describe('#PersonController', function() {

  describe('#getPersons', function() {
    it('should retrieve persons without error', function(done) {
      PersonController.getPersons(function(err,result){
        if(err){
          done(new Error(err.error));
          return;
        }
        done();
      });
    })
  })

  describe('#addPerson', function() {
    it('should add Person without error', function(done) {
      var newPerson = new Person({
        nin:'94533967024',
        name:'George VI',
        dateBirth:'1985-12-14'
      });
      PersonController.addPerson(newPerson, function(err,result){
        if(err||!result){
          done(new Error(err.error));
          return;
        }
        done();
      });
    });
  });

  describe('#addPerson', function() {
    it('should not permit add two persons with same nin', function(done) {
      var newPerson = new Person({
        nin:'26591009000',
        name:'George VI',
        dateBirth:'1985-12-14'
      });
      PersonController.addPerson(newPerson, function(err,result){
        if(err||!result){
          done(new Error(err.error));
          return;
        }
        PersonController.addPerson(newPerson, function(err,result){
          if(!err){
            done(new Error(err.error));
            return;
          }
          done();
        });
      });
    });
  });

  describe('#addPerson', function() {
    it('should not permit add Person without nin', function(done) {
      var newPerson = new Person({
        name:'George VI',
        dateBirth:'1985-12-14'
      });
      PersonController.addPerson(newPerson, function(err,result){
        if(!err){
          done(new Error(err.error));
          return;
        }
        done();
      });
    });
  });

  describe('#addPerson', function() {
    it('should not permit add Person without name', function(done) {
      var newPerson = new Person({
        nin:'28121742005',
        dateBirth:'1985-12-14'
      });
      PersonController.addPerson(newPerson, function(err,result){
        if(!err){
          done(new Error(err.error));
          return;
        }
        done();
      });
    });
  });

  describe('#addPerson', function() {
    it('should not permit add Person without date of birth', function(done) {
      var newPerson = new Person({
        nin:'19144662092',
        name:'George VI'
      });
      PersonController.addPerson(newPerson, function(err,result){
        if(!err){
          done(new Error(err.error));
          return;
        }
        done();
      });
    });
  });

  describe('#addAccount', function() {
    it('should permit add Conta', function(done) {
      var newPerson = new Person({
        nin:'26832319037',
        name:'George VI',
        dateBirth:'1985-12-14'
      });
      PersonController.addPerson(newPerson, function(err,person){
        if(err||!person){
          done(new Error(err.error));
          return;
        }

        PersonController.addAccount(person._id.toString(), 1000,1, function(err, resultInsConta){
          if(err||!resultInsConta){
            done(new Error(err.error));
            return;
          }
          done();
        });
      });
    });
  });

  describe('#perform Transaction', function() {
    it('should permit add transacao', function(done) {
      var newPerson = new Person({
        nin:'48586868094',
        name:'George VI',
        dateBirth:'1985-12-14'
      });
      PersonController.addPerson(newPerson, function(err,person){
        if(err||!person){
          console.log('1');
          done(new Error(err.error));
          return;
        }

        PersonController.addAccount(person._id.toString(), 1000,1, function(err, account){
          if(err||!account){
            console.log('12');
            done(new Error(err.error));
            return;
          }

          PersonController.performTransaction(
            person._id.toString()
            , account._id.toString()
            , 100
            , function(err, transaction){
              if(err||!transaction){
                console.log('123');
                done(new Error(err.error));
                return;
              }
              done();
            });
          });
        });
      });
    });

  })
