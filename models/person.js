var mongoose = require('mongoose');

var TransactionSchema = mongoose.Schema({
  value : { type : Number, required : true},
  date : { type : Date, required : true}
});

var AccountSchema = mongoose.Schema({
  balance : {type : Number, required : true},
  dailyWithdrawalLimit : { type : Number, required : true},
  active : { type : Boolean, required : true},
  type : { type : Number, required : true },
  creationDate : { type : Date, required : true, default: Date.now},
  transaction : [TransactionSchema]
});

var PersonSchema = mongoose.Schema({
  name : { type:String, required: true},
  // nin stands for nation identification number
  nin : { type:String , minlength:11, maxlength:11, required:true, unique:true},
  dateBirth : {type : Date,required:true},
  account: [AccountSchema]
});

module.exports = mongoose.model('Person', PersonSchema);
