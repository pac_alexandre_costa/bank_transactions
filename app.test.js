const app = require('./app');
var test = require('tape');
var request = require('supertest');

test('GET /api/v1/pessoa', function (assert) {
  request(app)
    .get('/api/v1/pessoa')
    .expect(200)
    .end(function (err, res) {
      assert.error(err, 'No error');
      assert.ok(res.body.length>0, 'List of persons');
      assert.end();
    });
});

test('GET /api/v1/pessoa/5b5f8d765afea3155faf2237', function (assert) {
  request(app)
    .get('/api/v1/pessoa/5b5f8d765afea3155faf2237')
    .expect(200)
    .end(function (err, res) {
      assert.error(err, 'No error');
      assert.end();
    });
});

test('POST /api/v1/pessoa', function (assert) {
  request(app)
    .post('/api/v1/pessoa')
    .send({
    	'cpf':'65964810359',
    	'nome':'paulo alexandre',
    	'dataNascimento':'1985/12/07'
    })
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .expect(200)
    .end(function (err, res) {
      assert.error(err, 'No error');
      assert.end();
    });
});
