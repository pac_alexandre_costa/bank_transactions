var Person = require('../models/person');
var mongoose = require('mongoose');

exports.getPersons = function (cb){
  Person.find({}, null, {sort:'name'}, function(err,result){
    return cb(err,result);
  });
};

exports.getPersonById = function (id,cb){
  Person
  .findById(id)
  .exec(function(err,result){
    return cb(err,result);
  });
};

exports.addPerson = function(person, cb ){
  var newPerson = new Person({
    nin:person.nin,
    name:person.name,
    dateBirth:person.dateBirth
  });
  newPerson.save(function(err,result){
    return cb(err,result);
  });
};

exports.addAccount = function (personId, dailyWithdrawalLimit, type, cb){
  Person.findById(
    personId,
    function(err,person){
      if(err||!person)
      return cb(err||!person,null);

      var newAccount = person.account.create({
        balance: 0,
        dailyWithdrawalLimit: dailyWithdrawalLimit,
        active: true,
        type : type
      });

      person.account.addToSet(newAccount);

      person.save(function(err){
        if(err)
        return cb(err,null)

        return cb(null,newAccount);
      });
    });
  };

  exports.getAccountById = function(personId, accountId, cb){
    Person.findById(personId, function(err,result){
      if(err||!result)
        return cb(err,null);
      var resultAccount = result.account.find(function(element){
        return element._id.equals(new mongoose.mongo.ObjectId(accountId));
      });
      return cb(err,resultAccount);
    });
  };

  exports.performTransaction = function(personId, accountId, transactionValue, cb){
    if(transactionValue === 0){
      return cb({"error":"Valor da transacao nao pode ser zero"}, null);
    }

    Person
    .findById(personId)
    .exec(function(err,person){

      if(err||!person)
        return cb(err,person);

      var account = person.account.find(function(element){
        return element._id.equals(new mongoose.mongo.ObjectId(accountId));
      });

      if(!account){
        return cb({'error':'Account not found.'}, null)
      }

      if(!account.active){
        return cb({"error":"Inactive account"}, null);
      }

      // This validation only applies to withdrawals
      // Withdrawals are only allowed if there is sufficient balance
      if(transactionValue < 0 && (account.balance + transactionValue) < 0 ){
        return cb({"error":"Insufficiente balance."}, null);
      }

      // Sum all withdrawals perform today
      const sumReducer =  function(sum, trans){
        var initialDate = new Date()
        initialDate.setHours(0,0,0,0);
        var finalDate = new Date();

        if(trans.value >= 0)
        return 0;

        if(trans.date < initialDate || trans.date > finalDate)
        return 0;

        return sum+Math.abs(trans.value);
      };

      var dailyWithdrawal = account.transaction.reduce(sumReducer,0);

      // This validation only applies to withdrawals
      if(transactionValue<0){
        if(dailyWithdrawal > account.dailyWithdrawalLimit ){
          return cb({"error":"Daily withdrawal limit already reached."},null);
        }
        if(dailyWithdrawal + Math.abs(transactionValue) > account.dailyWithdrawalLimit ){
          return cb({"error":"Transaction value surpasse daily withdraw lLimit."},null);
        }
      }

      var newTransaction = account.transaction.create({
        value : transactionValue,
        date : Date.now()
      });
      account.transaction.addToSet(newTransaction);
      account.balance += transactionValue;

      person.save(function(err){
        if(err)
          return cb(err, null)

        return cb(null, newTransaction);
      })

    });
  };
