var express = require('express');
var router = express.Router();
var controller = require('../controllers/person');

router.get('/', function(req,res){
  controller.getPersons(function(err,persons){
    if(err){
      res.status(400).send(err);
      return;
    }
    res.status(200).send(persons)
    return;
  });
});

router.get('/:id', function(req,res){
  controller.getPersonById(req.params.id, function(err,person){
    if(err){
      res.status(400).send(err);
      return;
    }
    if(!person){
      res.status(404).send();
      return;
    }
    res.status(200).send(person)
    return;
  });
});

router.post('/', function(req,res){
  var newPerson = {
    nin:req.body.nin,
    name:req.body.name,
    dateBirth: req.body.dateBirth
  };
  controller.addPerson(newPerson, function(err,person){
    if(err){
      res.status(400).send(err);
      return;
    }
    res.status(200).send(person)
    return;
  });
});

router.post('/:personId/account', function(req, res){

  controller.addAccount(
    req.params.personId,
    req.body.dailyWithdrawalLimit,
    req.body.type,
    function(err, account){
      if(err){
        res.status(400).send(err);
        return;
      }
      if(!account){
        res.status(404).send();
        return;
      }
      res.status(200).send(account)
      return;
    });
});

router.get('/:personId/account/:accountId', function(req,res){
    controller.getAccountById(
      req.params.personId,
      req.params.accountId,
      function(err,account){
        if(err){
          res.status(400).send(err);
          return;
        }
        if(!account){
          res.status(404).send();
          return;
        }
        res.status(400).send(account);
        return;
      });
    }
  );

router.post('/:personId/account/:accountId/transaction', function(req,res){
    controller.performTransaction(
      req.params.personId,
      req.params.accountId,
      req.body.value,
      function(err,result){
        if(err){
          res.status(400).send(err);
          return;
        }
        if(!result){
          res.status(404).send();
          return;
        }
        res.status(200).send(result);
        return;
      });
    }
  );


module.exports = router;
